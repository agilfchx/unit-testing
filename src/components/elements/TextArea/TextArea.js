import React from 'react';
import './TextArea.css';
import PropTypes from 'prop-types';

export default function TextArea({
  containerStyle,
  label,
  labelStyle,
  name,
  onChangeTextArea,
  placeholder,
  textAreatStyle,
}) {
  return (
    <div className={containerStyle}>
      <label className={labelStyle} htmlFor={name}>
        {label}
      </label>
      <textarea
        className={textAreatStyle}
        name={name}
        onChange={onChangeTextArea}
        placeholder={placeholder}
      />
    </div>
  );
}

TextArea.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChangeTextArea: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  textAreatStyle: PropTypes.string.isRequired,
};
