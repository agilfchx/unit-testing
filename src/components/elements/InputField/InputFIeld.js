import React from 'react';
import './InputField.css';
import PropTypes from 'prop-types';

export default function InputFIeld({
  containerStyle,
  inputStyle,
  isDisable,
  label,
  labelStyle,
  onChangeInput,
  placeholder,
  type,
  value,
}) {
  return (
    <div className={containerStyle}>
      <label className={labelStyle}>{label}</label>
      <input
        className={inputStyle}
        disabled={isDisable}
        onChange={onChangeInput}
        placeholder={placeholder}
        type={type}
        value={value}
      />
    </div>
  );
}

InputFIeld.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  inputStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChangeInput: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};
