import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { next } from '../../reducers/main';
import styles from './styles.scoped.css';
import { Link } from 'react-router-dom';

export default function Main() {
  const dispatch = useDispatch();
  const { idx, subtitles } = useSelector(s => s.main);

  const onClick = () => {
    dispatch(next(idx + 1));
  };

  return (
    <main className={styles.root}>
      <div className="text-black">
        <h1>Hello Telkom</h1>
        <p>{subtitles[idx]}</p>
        <button onClick={onClick}>next</button>
        <p>Maulana AKbar Ramadhan</p>
        {/* <Link to="/Belajar">Belajar</Link> */}
        <Link to="/Product">Produk</Link>
        <br/>
        <Link to="/Users">User</Link>
        <br/>
      </div>
    </main>
  );
}
