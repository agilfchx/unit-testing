/* eslint-disable no-console */
/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
import React, { useState, useEffect } from 'react';
import Table from '../../components/elements/Table/Table';
import data from './product.json';
import { Link, useLocation } from 'react-router-dom';
import Modal from '../../components/elements/Modal';
import Button from '../../components/elements/Button';
import Select from '../../components/elements/Select/Select';
import InputFIeld from '../../components/elements/InputField/InputFIeld';

export default function Products() {
  const [itemsData, setItemsData] = useState(data);
  const [category, setCategory] = useState('');
  const [isChecked, setIsChecked] = useState(true);

  const { state } = useLocation();

  useEffect(() => {
    setItemsData(state != null ? [state, ...itemsData] : [...itemsData]);
  }, []);

  const columns = [
    'Product Name',
    'Description',
    'Category',
    'Price',
    'Action',
  ];

  console.log(isChecked);

  const onChangeSelect = (e) => {
    const filteredData =
      e.target.value != ''
        ? isChecked
          ? data
              .filter((i) => i.category === e.target.value)
              .filter(
                (i) =>
                  i.expiryDate > new Date().toISOString().slice(0, 10) ||
                  i.expiryDate === null
              )
          : data.filter((i) => i.category === e.target.value)
        : isChecked
        ? data.filter(
            (i) =>
              i.expiryDate > new Date().toISOString().slice(0, 10) ||
              i.expiryDate === null
          )
        : data;
    setCategory(e.target.value);
    setItemsData(filteredData);
  };

  const onCheckHideExpired = () => {
    const filteredData = isChecked
      ? category == ''
        ? data.filter(
            (i) =>
              i.expiryDate > new Date().toISOString().slice(0, 10) ||
              i.expiryDate === null
          )
        : data
            .filter(
              (i) =>
                i.expiryDate > new Date().toISOString().slice(0, 10) ||
                i.expiryDate === null
            )
            .filter((i) => i.category === category)
      : category != ''
      ? data.filter((i) => i.category === category)
      : data;
    setIsChecked(!isChecked);
    setItemsData(filteredData);
  };

  const onDelete = (id) => {
    const newData = data.map((i) => {
      if (i.id === id) {
        i.isDeleted = true;
      }
    });
    setItemsData(newData);
  };

  const rowsData = (i, idx) => {
    return (
      <>
        {!i.isDeleted && (
          <tr
            className={`p-2 ${
              i.expiryDate < new Date().toISOString().slice(0, 10)
                ? 'opacity-40'
                : ''
            }`}
            key={idx}
          >
            <Link to={`/ProductDetail/${i.id}`}>
              <td className="p-2 flex just items-center w-72">
                <img className="rounded-full mr-3" src={i.image} width={70} />
                {i.name}
              </td>
            </Link>
            <td className="p-2">{i.description}</td>
            <td className="p-2 w-32">{i.category}</td>
            <td className="p-2 text-green-400 w-32">
              Rp. {i.price.toLocaleString('id-ID') + i.isDeleted}
            </td>
            <td className="p-2 text-center">
              <Modal onDelete={() => onDelete(i.id)}>
                Do you want to delete {i.name}?
              </Modal>
            </td>
          </tr>
        )}
      </>
    );
  };

  return (
    <div className="p-8 bg-white h-full overflow-y-auto">
      <div className="flex justify-between">
        <h4 className="mb-4 text-2xl font-bold text-[#1E293B]">Products</h4>
        <div className="flex">
          <InputFIeld
            containerStyle="checkbox"
            inputStyle="checkbox__text"
            label="Hide expired product"
            labelStyle="checkbox__label"
            onChangeInput={onCheckHideExpired}
            type="checkbox"
            value={isChecked}
          />
          <Select
            containerStyle="select"
            defaultValue="Chose Category"
            labelStyle="select__label"
            onChangeSelect={onChangeSelect}
            selectStyle="select__items"
            value={category}
          />
          <Button
            dest="/Products/AddNewProduct"
            style="btn btn__normal btn--custom"
            type={null}
          >
            Add New Product
          </Button>
        </div>
      </div>
      <hr className="mb-4" />
      <Table columns={columns} items={itemsData} rows={rowsData} />
    </div>
  );
}
