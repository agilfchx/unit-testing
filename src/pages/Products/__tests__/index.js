import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import ShallowRenderer from 'react-test-renderer/shallow';
import Product from '../Products';
import data from '../product.json';

const dummyProduct = {
  id: 1,
  name: 'Product 1',
  price: '123',
  description: 'Deskripsi',
  image: 'image.png',
  category: 'Category 1',
  expiryDate: '2022-01-01',
  isDeleted: false,
};

describe('src/pages/Products', () => {
  test('test render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Product />);
    expect(tree).toMatchSnapshot();
  });

  test('when render have state from useLocation', () => {
    useLocation.mockImplementation(() => ({
      state: dummyProduct,
    }));
    // setItem digunakan untuk expect() dan disisipkan di useState agar bisa masuk kedalam kode
    const setItem = jest.fn();
    useState.mockImplementation((v) => [v, setItem]);

    // Invoke
    Product();

    expect(setItem).toHaveBeenCalledWith([dummyProduct, ...data]);
  });

  test('render rowData with & without opacity', () => {
    const product = Product();
    const table = product.props.children[2];

    const withOpacity = table.props.rows(data[0], 0);
    expect(withOpacity.props.children.props.className).toBe('p-2 opacity-40');

    const withoutOpacity = table.props.rows(data[1], 1);
    /*
    butuh trim() karena terdapat whitespace
    Expected: "p-2"
    Received: "p-2 "
    */
    expect(withoutOpacity.props.children.props.className.trim()).toBe('p-2');
  });

  test('should trigger onDelete when delete button is clicked', () => {
    const setItems = jest.fn();
    useState.mockImplementationOnce((v) => [v, setItems]);

    const product = Product();
    const table = product.props.children[2];
    const rows = table.props.rows(data[0], 0);
    const modal = rows.props.children.props.children[4].props.children;

    // invoke onDelete
    modal.props.onDelete();

    // expect setItems ketika dipanggil dan terdapat object pada suatu array
    expect(setItems).toHaveBeenCalledWith(
      expect.arrayContaining([
        expect.objectContaining({ id: data[0].id, isDeleted: true }),
      ])
    );
  });
});
